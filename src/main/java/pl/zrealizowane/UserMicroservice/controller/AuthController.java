package pl.zrealizowane.UserMicroservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pl.zrealizowane.UserMicroservice.exceptions.BadRequestException;
import pl.zrealizowane.UserMicroservice.model.AuthProvider;
import pl.zrealizowane.UserMicroservice.model.User;
import pl.zrealizowane.UserMicroservice.payload.ApiResponse;
import pl.zrealizowane.UserMicroservice.payload.AuthResponse;
import pl.zrealizowane.UserMicroservice.payload.LoginRequest;
import pl.zrealizowane.UserMicroservice.payload.SignUpRequest;
import pl.zrealizowane.UserMicroservice.repository.UserRepository;
import pl.zrealizowane.UserMicroservice.security.TokenProvider;
import pl.zrealizowane.UserMicroservice.service.AuthService;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        return ResponseEntity.ok(new AuthResponse(authService.getUserToken(loginRequest)));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
        return ResponseEntity.created(
                authService.redirectToUserProfile(authService.registerNewUser(signUpRequest)))
                .body(new ApiResponse(true, "User registered successfully@"));
    }

}

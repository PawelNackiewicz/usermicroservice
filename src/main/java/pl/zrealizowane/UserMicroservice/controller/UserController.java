package pl.zrealizowane.UserMicroservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.zrealizowane.UserMicroservice.dto.UserDto;
import pl.zrealizowane.UserMicroservice.exceptions.ResourceNotFoundException;
import pl.zrealizowane.UserMicroservice.mapper.UserMapper;
import pl.zrealizowane.UserMicroservice.repository.UserRepository;
import pl.zrealizowane.UserMicroservice.security.CurrentUser;
import pl.zrealizowane.UserMicroservice.security.UserPrincipal;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/user/me")
    @PreAuthorize("hasRole('USER')")
    public UserDto getCurrentUser(@CurrentUser UserPrincipal userPrincipal) {
        UserDto userDto = UserMapper.INSTANCE.mapToUserDto(userRepository.findById(userPrincipal.getId())
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", userPrincipal.getId())));
        return userDto;
    }
}

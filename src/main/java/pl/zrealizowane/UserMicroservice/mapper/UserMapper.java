package pl.zrealizowane.UserMicroservice.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.zrealizowane.UserMicroservice.dto.UserDto;
import pl.zrealizowane.UserMicroservice.model.User;

public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserDto mapToUserDto(User user);
}

package pl.zrealizowane.UserMicroservice.mapper;

import pl.zrealizowane.UserMicroservice.dto.UserDto;
import pl.zrealizowane.UserMicroservice.model.User;

public class UserMapperImpl implements UserMapper {
    @Override
    public UserDto mapToUserDto(User user) {
        if(user == null) {
            return null;
        }
        UserDto userDto = new UserDto(user.getFirstName(), user.getLastName(), user.getEmail(), user.getImageUrl());
        return userDto;
    }
}

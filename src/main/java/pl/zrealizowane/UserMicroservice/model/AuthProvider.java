package pl.zrealizowane.UserMicroservice.model;

public enum  AuthProvider {
    local,
    facebook,
    google
}